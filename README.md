<!--
## Bem-vindo ao meu portfólio.
-->
## Social

[![Insta](https://img.shields.io/badge/jprs.drw-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/jprs.drw/)
[![Linkedin](https://img.shields.io/badge/joao_prs-0E76A8?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/joao-prs/)

- 🌱 I’m currently learning Cloud AWS, Kubernetes and Golang
- 📫 How to reach me: jao.prss@gmail.com
- 🏅 See my 👉 <a href="https://www.credly.com/users/joao-prs">badges</a> 👈

## Skills

![Linux](https://img.shields.io/badge/-Linux-FCC624?style=flat-square&logo=linux&logoColor=black)
![Manjaro](https://img.shields.io/badge/-Manjaro-35BF5C?style=flat-square&logo=manjaro&logoColor=white)
![Arch](https://img.shields.io/badge/-Arch-168eca?style=flat-square&logo=arch-linux&logoColor=white)
![redhat](https://img.shields.io/badge/-RedHat-da1414?style=flat-square&logo=redhat&logoColor=white)
![Proxmox](https://img.shields.io/badge/-Proxmox-db7716?style=flat-square&logo=proxmox&logoColor=white)
![Docker](https://img.shields.io/badge/-Docker-46a2f1?style=flat-square&logo=docker&logoColor=white)
![Terraform](https://img.shields.io/badge/-Terraform-844FBA?style=flat-square&logo=terraform&logoColor=white)
![Git](https://img.shields.io/badge/-Git-F05032?style=flat-square&logo=git&logoColor=white)
![aws](https://img.shields.io/badge/-AWS-ff9538?style=flat-square&logo=amazon&logoColor=white)
![kubernetes](https://img.shields.io/badge/-Kubernetes-3887ff?style=flat-square&logo=kubernetes&logoColor=white)
![nginx](https://img.shields.io/badge/-Nginx-08bf0e?style=flat-square&logo=nginx&logoColor=white)
![Mariadb](https://img.shields.io/badge/-MariaDB-003545?style=flat-square&logo=mariadb&logoColor=white) 
![Postgresql](https://img.shields.io/badge/-Postgresql-427db5?style=flat-square&logo=postgresql&logoColor=white)
![PHP](https://img.shields.io/badge/-php-775ec2?style=flat-square&logo=php&logoColor=white)
![Python](https://img.shields.io/badge/-Python-ffec29?style=flat-square&logo=python&logoColor=black)
![Golang](https://img.shields.io/badge/-Go-00ADD8?style=flat-square&logo=go&logoColor=white)
![Lua](https://img.shields.io/badge/-Lua-000080?style=flat-square&logo=lua&logoColor=white)
![Javascript](https://img.shields.io/badge/-JavaScript-fff519?style=flat-square&logo=javascript&logoColor=black)
![PS](https://img.shields.io/badge/-Photoshop-31A8FF?style=flat-square&logo=Adobe%20Photoshop&logoColor=white)
![Blender](https://img.shields.io/badge/-Blender-ed961e?style=flat-square&logo=blender&logoColor=white)

- 🧰 Tools : Linux • Bash Script • Go • Ansible • Terraform
- ☁️ Clouds : Openstack • Proxmox • AWS • Docker • Kubernetes
